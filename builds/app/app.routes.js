"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const home_component_1 = require("./home/home.component");
const router_1 = require("@angular/router");
const login_component_1 = require("./login/login.component");
const auth_guard_1 = require("./common/auth.guard");
// Route Configuration
exports.routes = [
    { path: '', component: home_component_1.HomeComponent, canActivate: [auth_guard_1.AuthGuard] },
    { path: 'home', component: home_component_1.HomeComponent, canActivate: [auth_guard_1.AuthGuard] },
    { path: 'login', component: login_component_1.LoginComponent },
];
exports.routing = router_1.RouterModule.forRoot(exports.routes);

//# sourceMappingURL=app.routes.js.map
