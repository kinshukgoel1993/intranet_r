"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const header_component_1 = require("./common/header.component");
const alert_component_1 = require("./_directives/alert.component");
const authentication_service_1 = require("./_services/authentication.service");
const auth_guard_1 = require("./common/auth.guard");
const home_component_1 = require("./home/home.component");
const core_1 = require("@angular/core");
const platform_browser_1 = require("@angular/platform-browser");
const router_1 = require("@angular/router");
const http_1 = require("@angular/http");
const forms_1 = require("@angular/forms");
const common_1 = require("@angular/common");
const app_component_1 = require("./app.component");
const login_component_1 = require("./login/login.component");
const app_routes_1 = require("./app.routes");
const alert_service_1 = require("./_services/alert.service");
let AppModule = class AppModule {
};
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            router_1.RouterModule,
            app_routes_1.routing,
            http_1.HttpModule,
            forms_1.FormsModule
        ],
        declarations: [
            app_component_1.AppComponent,
            header_component_1.HeaderComponent,
            alert_component_1.AlertComponent,
            home_component_1.HomeComponent,
            login_component_1.LoginComponent
        ],
        providers: [{ provide: common_1.LocationStrategy, useClass: common_1.HashLocationStrategy }, auth_guard_1.AuthGuard, alert_service_1.AlertService, authentication_service_1.AuthenticationService],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;

//# sourceMappingURL=app.module.js.map
