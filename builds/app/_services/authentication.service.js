"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = require("@angular/http");
const router_1 = require("@angular/router");
const core_1 = require("@angular/core");
require("rxjs/add/operator/map");
let AuthenticationService = class AuthenticationService {
    constructor(router, http) {
        this.router = router;
        this.http = http;
    }
    logout() {
        console.log("REACHED LOGOUT");
        localStorage.removeItem("id_token");
        localStorage.removeItem("username");
        this.router.navigate(['login']);
    }
    login(emailId, password) {
        console.log(btoa("username:temppass"));
        var authdata = btoa(emailId + ':' + password);
        const contentHeaders = new http_1.Headers();
        contentHeaders.append('Authorization', 'Basic ' + authdata);
        console.log(emailId);
        console.log(password);
        this.http.post('/login', { username: emailId, password: password }, { headers: contentHeaders })
            .subscribe(response => {
            console.log("checking the response", response);
            if (JSON.stringify(response.json().success) === "true") {
                localStorage.setItem('id_token', JSON.stringify(response.json().token));
                localStorage.setItem('username', JSON.stringify(response.json().username));
                this.router.navigate(['home']);
            }
            else {
                console.log(JSON.stringify(response.json().message));
            }
        }, error => {
            console.log(error.text());
            return false;
        });
    }
};
AuthenticationService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [router_1.Router, http_1.Http])
], AuthenticationService);
exports.AuthenticationService = AuthenticationService;

//# sourceMappingURL=authentication.service.js.map
