(function (api) {
    var authApi = require("./authApi.js");


    api.init = function (app) {
        authApi.init(app);
    };

})(module.exports);