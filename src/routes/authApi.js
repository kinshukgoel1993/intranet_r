(function (auth) {
    var passport = require('passport');
    var jwt = require('jsonwebtoken');
    var BasicStrategy = require('passport-http').BasicStrategy;
    var BearerStrategy = require('passport-http-bearer').Strategy;
    var crypto = require('crypto');
    var secret = require('../config/config')
    var userDb = require('../../development/db/user');
    auth.init = function (app) {

        var secretKey = secret.secret ;



        passport.use(new BasicStrategy(
            function (username, password, next) {


                userDb.getOne(username).then(function (dbUser) {
                    if (!dbUser) {
                        return next(null, { 'error': global.constants.USER_NOT_FOUND });
                    } else {

                        const hash = crypto.createHmac('sha256', dbUser.password_salt)
                            .update(password)
                            .digest('hex');
                        if (hash === dbUser.password) {
                            var userObject = dbUser.toObject();
                            delete userObject.password_salt;
                            delete userObject.password;
                            delete userObject._id;

                            return next(null, userObject);
                        } else {
                            return next(null, { 'error': global.constants.INVALID_USER_PASSWORD });
                        }
                    }
                }).catch(function (error) {
                    return next(error);
                });
            }));

        passport.use(new BearerStrategy(function (token, next) {

            console.log("BEARER STRAGEY", token);
            jwt.verify(token, secretKey, function (err, decoded) {
                if (err) {
                    console.log(" ERROR BEARER STRAGEY");
                    return done(err, null);
                } else {
                    console.log("BEARER STRAGEY");
                    userDb.getOne(decoded.user.email_id).then(function (response) {
                        return next(null, response);
                    }).catch(function (err) {
                        return next(err);
                    });
                }
            });
        }));

        app.post('/login', function (req, res) {
            console.log("calling api  model")
            if (req.user.error) {

                res.status(401).json(req.user);
                res.end();
            }
            else {

                //create jwt token here
                var secretKey = secret.secret;
                var token = jwt.sign({
                    'user': req.user,
                    'iat': Math.floor(Date.now() / 1000) - 30,
                    'expiresIn': '1d',
                }, secretKey);
                if (token) {
                    res.json({
                        success: true,
                        message: 'Enjoy your token!',
                        token: 'jwt ' + token,
                        username: req.user.email_id
                    });
                }
                else {
                    res.status(500).json({ 'error': 'Error in generating the json web token' });
                }
            }

        });

    };
})(module.exports); 