(function (user) {

    var mongoose = require('mongoose');
    var crypto = require('crypto');
    var Q = require('q');
    mongoose.Promise = require('q').Promise;

    var Schema = mongoose.Schema;

    //Define the schema for user object
    var userSchema = new mongoose.Schema({
        account: { type: Schema.Types.ObjectId, ref: 'accounts' },
        first_name: String,
        last_name: String,
        phone_number: String,
        email_id: String,
        password_salt: String,
        password: String,
        role: String,
        is_active: Boolean,
        last_updated_on: Date,
        last_updated_by: String
    });

    //email_id is unique. This will help in performing upsert
    userSchema.index({ email_id: 1 }, { unique: true });
    var userModel = mongoose.model('users', userSchema);

    user.changePassword = function (passwordData, next) {

    };

    user.getProfile = function (_id, next) {
        //Make sure no salt, id and password fields are returned.
    }

    user.getAll = function () {
        var query = userModel.find({}, { _id: 0, password: 0, password_salt: 0 });
        return query.exec();
        //Make sure no salt, id and password fields are returned.
    }

    user.getOne = function (userName) {
        console.log("mongo check", userName)
        var query = userModel.findOne({ 'email_id': userName });
        var check  =query.exec();
        return query.exec();
    }

    user.delete = function (userName) {
   
        var query = userModel.remove({ 'email_id': userName});
        return query.exec();
    }

    user.seedData = function () {
        return userModel.count(function (error, result) {
            if (error) {
             

            } else {
           
                if (result == 0) {
                    var userObj = {
                        'first_name': 'Super',
                        'last_name': 'Admin',
                        'phone_number': '+919972731751',
                        'email_id': 'kinshukgoel93@gmail.com',
                        'password': 'Ghar@331',
                        'is_active': true,
                        'last_updated_on': new Date(),
                        'last_updated_by': 'system',
                        'account_name': 'superadmin',
                        'role': 'sa'
                    };
                    var salt = crypto.randomBytes(16).toString('hex');
                    console.log("SALT", salt);
                    var passwordHash = crypto.createHmac('sha256', salt).update(userObj.password).digest('hex');
                    console.log("PASSWORD HASH", passwordHash)
                    var userData = {
                        'first_name': userObj.first_name,
                        'last_name': userObj.last_name,
                        'phone_number': userObj.phone_number,
                        'email_id': userObj.email_id,
                        'password_salt': salt,
                        'password': passwordHash,
                        'role': userObj.role,
                        'is_active': userObj.is_active,
                        'last_updated_on': new Date(),
                        'last_updated_by': userObj.last_updated_by,
                    }
                    

                    console.log(userData);
                    userModel.insertMany(userData);
                  
                }
            }
        });
    };


})(module.exports);