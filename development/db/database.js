var mongoose = require('mongoose');
var userDb = require('./user');
// Build the connection string

var dbURI = 'mongodb://' + "localhost" + ':' + "27017" + '/' + "Intranet";
console.log(dbURI);

// Create the database connection
mongoose.connect(dbURI);

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.once('open', function () {
    console.log('Mongoose default connection open to ' + dbURI);
    userDb.seedData().then(function(response){
        console.log(response);
        console.log('Seeding complete');
    });
});

// If the connection throws an error
mongoose.connection.on('error', function (err) {
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function () {
    mongoose.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});
