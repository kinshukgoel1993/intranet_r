import {Component} from '@angular/core';

@Component({
selector:"my-app",
moduleId:module.id,
template:`
<app-header></app-header>
<router-outlet></router-outlet>`
})
export class AppComponent{

}
