import { AuthenticationService } from './../_services/index';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-header',
  moduleId: module.id,
  templateUrl: '../../partials/header-template.html'
})
export class HeaderComponent implements OnInit {
  model: any = {};
  isLogged =false;
  loading = false;
  returnUrl: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    ) { }

  ngOnInit() {
    if(localStorage.getItem('username')){
      this.isLogged = true;
    }
  }
  
}