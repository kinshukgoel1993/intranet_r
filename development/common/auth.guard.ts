import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router) { }

  canActivate() {

    console.log(localStorage.getItem('username'))

    if(localStorage.getItem('username')){
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }
}