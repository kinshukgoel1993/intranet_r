import { AlertService, AuthenticationService } from './../_services/index';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';



@Component({
  moduleId: module.id,
  templateUrl: '../../partials/home.component.html'
})
export class HomeComponent implements OnInit {
  model: any = {};
  loading = false;
  returnUrl: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService) { }

  ngOnInit() {

  }


  logout() {
    console.log("Clicked Logout")
    this.authenticationService.logout()
  }

  RedirectToHome() {
    console.log("Home Clicked")
    this.router.navigate(['home']);
  }


}