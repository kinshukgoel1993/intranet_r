import { HeaderComponent } from './common/header.component';
import { AlertComponent } from './_directives/alert.component';
import { AuthenticationService } from './_services/authentication.service';
import { AuthGuard } from './common/auth.guard';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { routing } from './app.routes';
import { AlertService } from './_services/alert.service';
@NgModule({
    imports: [
        BrowserModule, 
        RouterModule, 
        routing, 
        HttpModule,
        FormsModule
    ],

    declarations: [ 
        AppComponent,
        HeaderComponent, 
        AlertComponent,
        HomeComponent, 
        LoginComponent],
    providers : [{ provide: LocationStrategy, useClass: HashLocationStrategy },AuthGuard, AlertService, AuthenticationService],
    bootstrap: [AppComponent]
})
export class AppModule { }
