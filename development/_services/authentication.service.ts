import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
    constructor(private router: Router, private http: Http) {
    }

    logout() {
        console.log("REACHED LOGOUT")
        localStorage.removeItem("id_token");
        localStorage.removeItem("username");
        this.router.navigate(['login']);
    }

    login(emailId: string, password: string) {
        console.log(btoa("username:temppass"));
        var authdata = btoa(emailId + ':' + password)
        const contentHeaders = new Headers();

        contentHeaders.append('Authorization', 'Basic ' + authdata); 
        console.log(emailId)
        console.log(password)
        this.http.post('/login', { username: emailId, password: password }, { headers: contentHeaders })
            .subscribe(
                response => {

                    console.log("checking the response", response);

                    if (JSON.stringify(response.json().success) === "true") {
                        localStorage.setItem('id_token', JSON.stringify(response.json().token));
                        localStorage.setItem('username', JSON.stringify(response.json().username));
                        this.router.navigate(['home']);
                    }
                    else {
                        console.log(JSON.stringify(response.json().message))
                    }
                },
                error => {
                    console.log(error.text());
                    return false;
                });

    }


}